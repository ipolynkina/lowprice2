package ru.ipolynkina.lowPrice2.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.ipolynkina.lowPrice2.domain.Role;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepo userRepo;

    @MockBean
    private MailSender mailSender;

    private User user = new User("u", "usr@usr.ru", "u");

    @Test
    public void loadUserByUsernameTest() {
        Mockito.doReturn(new User()).when(userRepo).findByUsername(ArgumentMatchers.anyString());
        userService.loadUserByUsername("");

        Mockito.verify(userRepo, Mockito.times(1)).findByUsername(ArgumentMatchers.anyString());
    }

    @Test
    public void addUserTest() {
        userService.addUser(user);

        Mockito.verify(userRepo, Mockito.times(1)).save(user);
        Assert.assertTrue(user.getRoles().contains(Role.USER));
        Assert.assertFalse(user.getRoles().contains(Role.ADMIN));
        Assert.assertNotNull(user.getActivationCode());
    }

    @Test
    public void activateUserTest() {
        user.setActivationCode("");

        Mockito.doReturn(user).when(userRepo).findByActivationCode(ArgumentMatchers.anyString());
        userService.activateUser("");

        Mockito.verify(userRepo, Mockito.times(1)).findByActivationCode(ArgumentMatchers.anyString());
        Mockito.verify(userRepo, Mockito.times(1)).save(user);
        Assert.assertNull(user.getActivationCode());
        Assert.assertTrue(user.isUserActive());
    }

    @Test
    public void setMailingActiveTest() {
        Mockito.doReturn(user).when(userRepo).findByEmail(ArgumentMatchers.anyString());
        userService.setMailingActive(user, false);

        Mockito.verify(userRepo, Mockito.times(1)).findByEmail(ArgumentMatchers.anyString());
        Mockito.verify(userRepo, Mockito.times(1)).save(user);
        Assert.assertFalse(user.isMailingActive());
    }

    @Test
    public void sendActivationCodeTest() throws Exception {
        userService.sendActivationCode(user);

        Mockito.verify(mailSender, Mockito.times(1)).send(ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
    }

    @Test
    public void sendRecoveryCodeTest() throws Exception {
        userService.sendRecoveryCode(user);

        Mockito.verify(mailSender, Mockito.times(1)).send(ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
    }

    @Test
    public void sendFeedbackTest() throws Exception {
        userService.sendFeedback(user, "");

        Mockito.verify(mailSender, Mockito.times(1)).send(ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
        Assert.assertEquals(1, user.getMessagesCounter());
    }

    @Test
    public void sendPricingInformationTest() throws Exception {
        userService.sendPricingInformation(user, "");

        Mockito.verify(mailSender, Mockito.times(1)).send(ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
    }
}
