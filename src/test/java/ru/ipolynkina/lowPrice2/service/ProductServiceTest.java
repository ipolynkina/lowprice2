package ru.ipolynkina.lowPrice2.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.ipolynkina.lowPrice2.domain.Product;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.ProductRepo;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @MockBean
    private ProductRepo productRepo;

    @MockBean
    private UserRepo userRepo;

    private User user = new User("user", "user@user.ru", "user");
    private Product product = new Product("Холодильник", "eldorado.ru", 25000.0, user);

    @Test
    public void addProductTest() {
        productService.addProduct(product, user);

        Mockito.verify(productRepo, Mockito.times(1)).save(product);
        Assert.assertEquals(1, user.getProductsCounter());
    }

    @Test
    public void deleteProductTest() {
        Mockito.doReturn(product).when(productRepo).findById(ArgumentMatchers.anyLong());

        productService.addProduct(product, user);
        productService.deleteProductById(1L);

        Mockito.verify(productRepo, Mockito.times(1)).findById(1L);
        Mockito.verify(productRepo, Mockito.times(1)).delete(product);
        Assert.assertEquals(0, user.getProductsCounter());
    }
}
