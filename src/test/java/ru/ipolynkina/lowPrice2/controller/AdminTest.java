package ru.ipolynkina.lowPrice2.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/user-before.sql", "/product-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/product-after.sql", "/user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@WithUserDetails(value = "dmn")
public class AdminTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void pageLoadTest() throws Exception {
        mockMvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("dmn")))
                .andExpect(content().string(containsString("Выйти</button>")))

                .andExpect(content().string(containsString("<h2>Список")))
                .andExpect(content().string(containsString("Найти по email</button>")))

                .andExpect(content().string(containsString("Действия</th>")))
                .andExpect(content().string(containsString("Выключить</button>")))
                .andExpect(content().string(containsString("Удалить</button>")));
    }

    @Test
    public void userListTest() throws Exception {
        mockMvc.perform(get("/users"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='userList']").nodeCount(2));
    }

    @Test
    public void filterByEmail() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/filterByEmail")
                .param("filter", "dmn@dmn.ru")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='userList']").nodeCount(1));

        multipart = multipart("/filterByEmail")
                .param("filter", "111@111.ru")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='userList']").nodeCount(2));
    }

    @Test
    public void changeStatus() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/changeStatus")
                .param("username", "u")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='status/2']").string("Нет"));
    }

    @Test
    public void deleteAccount() throws  Exception {
        MockHttpServletRequestBuilder multipart = multipart("/deleteAccount")
                .param("username", "u")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='userList']").nodeCount(1));
    }
}
