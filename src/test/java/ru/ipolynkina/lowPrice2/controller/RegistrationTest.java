package ru.ipolynkina.lowPrice2.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.client.RestTemplate;
import ru.ipolynkina.lowPrice2.domain.dto.CaptchaResponseDto;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
public class RegistrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private CaptchaResponseDto response;

    @Test
    public void pageLoadTest() throws Exception {
        mockMvc.perform(get("/registration"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("placeholder=\"Логин\"")))
                .andExpect(content().string(containsString("placeholder=\"Email\"")))
                .andExpect(content().string(containsString("placeholder=\"Пароль\"")))
                .andExpect(content().string(containsString("placeholder=\"Пароль (еще раз)\"")))
                .andExpect(content().string(containsString("Зарегистрироваться</button>")));
    }

    @Test
    @Sql(value = {"/user-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void registrationTest() throws Exception {
        Mockito.doReturn(response)
                .when(restTemplate)
                .postForObject(
                        ArgumentMatchers.anyString(),
                        ArgumentMatchers.any(),
                        ArgumentMatchers.any()
                );

        Mockito.doReturn(true)
                .when(response)
                .isSuccess();

        MockHttpServletRequestBuilder multipart = multipart("/registration")
                .param("username", "u")
                .param("email", "usr@susr.ru")
                .param("g-recaptcha-response", "")
                .param("password", "u")
                .param("password2", "u")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(content().string(containsString("Пользователь с таким именем уже существует")));
    }

    @Test
    public void activateByCodeTest() throws Exception {
        mockMvc.perform(get("/activate/456"))
                .andDo(print())
                .andExpect(content().string(containsString("Регистрация завершена успешно")));

        mockMvc.perform(get("/activate/555"))
                .andDo(print())
                .andExpect(content().string(containsString("Неверный код активации")));
    }
}
