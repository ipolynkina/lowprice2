package ru.ipolynkina.lowPrice2.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/user-before.sql", "/product-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/product-after.sql", "/user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@WithUserDetails(value = "u")
public class PasswordTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void pageLoadTest() throws Exception {
        mockMvc.perform(get("/password"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("placeholder=\"Новый пароль")))
                .andExpect(content().string(containsString("placeholder=\"Новый пароль (еще раз)")))
                .andExpect(content().string(containsString("Сохранить</button>")));
    }

    @Test
    public void changePasswordTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/changePassword")
                .param("password", "1")
                .param("password2", "1")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Пароль успешно изменен")));

        multipart = multipart("/changePassword")
                .param("password", "1")
                .param("password2", "2")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Пароли не идентичны")));
    }

    @Test
    public void changePasswordByCodeTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/changePassword")
                .param("password", "1")
                .param("password2", "1")
                .param("code", "789")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Пароль успешно изменен")));

        multipart("/changePassword")
                .param("password", "1")
                .param("password2", "1")
                .param("code", "555")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Код восстановления некорректный")));
    }
}
