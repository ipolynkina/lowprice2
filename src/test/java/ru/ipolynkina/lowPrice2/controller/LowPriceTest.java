package ru.ipolynkina.lowPrice2.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LowPriceTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void pageLoadTest() throws Exception {
        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Low Price")))
                .andExpect(content().string(containsString("Личный кабинет")))
                .andExpect(content().string(containsString("Обратная связь")))

                .andExpect(content().string(containsString("unknown")))
                .andExpect(content().string(containsString("Войти")))

                .andExpect(content().string(containsString("Планируете покупку")))
                .andExpect(content().string(containsString("время")))
                .andExpect(content().string(containsString("Вам")))
                .andExpect(content().string(containsString("Оповещение")))
                .andExpect(content().string(containsString("Возможно")));
    }
}
