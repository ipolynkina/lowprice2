package ru.ipolynkina.lowPrice2.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/user-before.sql", "/product-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/product-after.sql", "/user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@WithUserDetails(value = "u")
public class AccountTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void pageLoadTest() throws Exception {
        mockMvc.perform(get("/account"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("u")))
                .andExpect(content().string(containsString("Выйти")))

                .andExpect(content().string(containsString("Товары")))
                .andExpect(content().string(containsString("placeholder=\"Вставьте ссылку")))
                .andExpect(content().string(containsString("Добавить</button>")))

                .andExpect(content().string(containsString("<th scope=\"col\">Действие</th>")))
                .andExpect(content().string(containsString("Удалить")));
    }

    @Test
    public void productListTest() throws Exception {
        mockMvc.perform(get("/account"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='productList']").nodeCount(4));
    }

    @Test
    public void addProductTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/addProduct")
                .param("link", "https://www.citilink.ru/catalog/car_electronics/car_receivers/1121619/")
                .param("user", "u")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='productList']").nodeCount(5));
    }

    @Test
    public void deleteProductTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/deleteProduct")
                .param("user", "u")
                .param("id", "2")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='productList']").nodeCount(3));
    }

    @Test
    public void stopMailingTest() throws Exception {
        mockMvc.perform(get("/stopMailing/123"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Вы успешно отписались от рассылки")));

        mockMvc.perform(get("/stopMailing/555"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("Неверный код")));
    }

    @Test
    public void usersAccessDeniedTest() throws Exception {
        this.mockMvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }
}
