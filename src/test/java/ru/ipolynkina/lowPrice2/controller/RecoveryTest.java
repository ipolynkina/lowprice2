package ru.ipolynkina.lowPrice2.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/user-before.sql", "/product-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/product-after.sql", "/user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RecoveryTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void pageLoadTest() throws Exception {
        mockMvc.perform(get("/recovery"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Укажите свой email, и мы")))
                .andExpect(content().string(containsString("placeholder=\"Укажите свой email\"")))
                .andExpect(content().string(containsString("Отправить</button>")));
    }

    @Test
    public void recoveryByEmailTest() throws Exception {
        MockHttpServletRequestBuilder multipart = multipart("/recovery")
                .param("email", "111@111.ru")
                .with(csrf());

        mockMvc.perform(multipart)
                .andDo(print())
                .andExpect(content().string(containsString("Пользователь с таким email не найден")));
    }

    @Test
    public void recoveryByCodeTest() throws Exception {
        mockMvc.perform(get("/recovery/789"))
                .andDo(print())
                .andExpect(content().string(containsString("789")));

        mockMvc.perform(get("/recovery/555"))
                .andDo(print())
                .andExpect(content().string(containsString("Код восстановления некорректный")));
    }
}
