package ru.ipolynkina.lowPrice2.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/user-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@WithUserDetails(value = "u")
public class FeedbackTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void pageLoadTest() throws Exception {
        mockMvc.perform(get("/feedback"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("u")))
                .andExpect(content().string(containsString("Выйти")))

                .andExpect(content().string(containsString("Сегодня")))
                .andExpect(content().string(containsString("textarea")))
                .andExpect(content().string(containsString("Отправить</button>")))
                .andExpect(xpath("//*[@id='amountMessages']")
                        .string("Сегодня отправлено сообщений: 0 "));
    }

    @Test
    @Sql(value = {"/user-before.sql", "/user-send-message.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void sendFeedbackTest() throws Exception {
        mockMvc.perform(get("/feedback"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("//*[@id='amountMessages']")
                        .string("Сегодня отправлено сообщений: 1 "));
    }
}
