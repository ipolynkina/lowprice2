delete from user_role;
delete from usr;

insert into usr(id, username, email, password, user_active, recovery_code, activation_code,
    mailing_active, mailing_code, products_counter, messages_counter, vip_level) values
    (1, 'dmn', 'dmn@dmn.ru', 'dmn', true, '000', '222', true, '111', 0, 0, 0),
    (2, 'u', 'usr@usr.ru', 'u', true, '789', '456', true, '123', 0, 0, 0);

insert into user_role(user_id, roles) values
    (1, 'ADMIN'),
    (2, 'USER');

create extension if not exists pgcrypto;
update usr set password = crypt(password, gen_salt('bf', 8));