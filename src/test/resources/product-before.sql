delete from product;

insert into product(id, name, link, price, user_id) values
    (1, 'Холодильник', 'eldorado.ru', 15000, 1),
    (2, 'Телевизор', 'eldorado.ru', 20000, 2),
    (3, 'Телевизор', 'dns.ru', 25000, 2),
    (4, 'Ст. машинка', 'mvideo.ru', 30000, 2),
    (5, 'Смартфон', 'mvideo.ru', 27000, 2);

alter sequence hibernate_sequence restart with 10;