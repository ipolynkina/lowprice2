package ru.ipolynkina.lowPrice2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailSender {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String lowPriceEmail;


    public void send(String emailTo, String subject, String text) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        message.setSubject(subject);

        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(lowPriceEmail);
        helper.setTo(emailTo);
        helper.setText(text, true);

        mailSender.send(message);
    }
}
