package ru.ipolynkina.lowPrice2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ipolynkina.lowPrice2.domain.Role;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

import javax.mail.MessagingException;
import java.util.Collections;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {

    @Value("${max_quantity_messages}")
    private int maxQuantityMessages;

    @Value("${spring.mail.username}")
    private String lowPriceEmail;

    @Value("${hostname}")
    private String hostname;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailSender mailSender;

    /* ---------------------------------------------- user ---------------------------------------------- */

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("Пользователь с таким логином не найден");
        }
        return user;
    }

    public boolean loginIsUnique(User user) {
        User userFromDb = userRepo.findByUsername(user.getUsername());
        return userFromDb == null;
    }

    public boolean emailIsUnique(User user) {
        User userFromDb = userRepo.findByEmail(user.getEmail());
        return userFromDb == null;
    }

    public void addUser(User user) {
        user.setRoles(Collections.singleton(Role.USER));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setActivationCode(UUID.randomUUID().toString());
        userRepo.save(user);
    }

    public boolean activateUser(String code) {
        User user = userRepo.findByActivationCode(code);

        if(user == null || user.getActivationCode() == null) {
            return false;
        }

        user.setActivationCode(null);
        user.setUserActive(true);
        userRepo.save(user);
        return true;
    }

    /* --------------------------------------------- product -------------------------------------------- */

    public int getAmountProducts(User user) {
        return user.getProductsCounter();
    }

    /* --------------------------------------------- message -------------------------------------------- */

    public boolean messageLimitExceeded(User user) {
        return user.getMessagesCounter() >= maxQuantityMessages;
    }

    public int getMaxQuantityMessages() {
        return maxQuantityMessages;
    }

    private void increaseMessageCounter(User user) {
        user.setMessagesCounter(user.getMessagesCounter() + 1);
        userRepo.save(user);
    }

    /* ----------------------------------------- sending email ----------------------------------------- */

    public void setMailingActive(User user, boolean active) {
        User currentUser = userRepo.findByEmail(user.getEmail());
        currentUser.setMailingActive(active);
        userRepo.save(user);
    }

    public void sendActivationCode(User user) throws MessagingException {
        String message = String.format("<p>%s, приветствуем Вас на LowPrice!</p>" +
                        "<br/>Для завершения регистрации перейдите по ссылке " +
                        "<a href=\"http://%s/activate/%s\" target=\"_blank\">Подтвердить email<a>",
                user.getUsername(),
                hostname,
                user.getActivationCode()
        );
        mailSender.send(user.getEmail(), "Activation code", message);
    }

    public void sendRecoveryCode(User user) throws MessagingException {
        String message = String.format("%s! <br/><br/>" +
                        "Для восстановления пароля на LowPrice пройдите по ссылке \n" +
                        "<a href=\"http://%s/recovery/%s\" target=\"_blank\">Ввести новый пароль</a>\n" +
                        "<br/>Восстановить пароль вы можете в течение 15 минут!",
                user.getUsername(),
                hostname,
                user.getRecoveryCode()
        );
        mailSender.send(user.getEmail(), "Recovery code", message);
    }

    public void sendFeedback(User user, String message) throws MessagingException {
        String messageText = String.format("Сообщение от пользователя %s " +
                        "<br/>С email: %s " +
                        "<br/><br/>%s",
                user.getUsername(),
                user.getEmail(),
                message
        );
        increaseMessageCounter(user);
        mailSender.send(lowPriceEmail,"feedback", messageText);
    }

    public void sendPricingInformation(User user, String message) throws MessagingException {
        message = message + String.format("<br/>Для отказа от рассылки LowPrice" +
                        " перейдите по ссылке <a href=\"http://%s/stopMailing/%s\" " +
                        "target=\"_blank\">остановить рассылку</a>" +
                        " Возобновить рассылку вы можете в любой момент," +
                        " изменив наблюдаемые товары в вашем личном кабинете",
                        hostname,
                        user.getMailingCode());
        mailSender.send(user.getEmail(),"Изменение цены", message);
    }
}
