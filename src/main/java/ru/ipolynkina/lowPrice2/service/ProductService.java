package ru.ipolynkina.lowPrice2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.ipolynkina.lowPrice2.domain.Product;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.ProductRepo;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

@Service
public class ProductService {

    @Value("${max_quantity_products}")
    private int maxQuantityProducts;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ProductRepo productRepo;

    public void addProduct(Product product, User user) {
        productRepo.save(product);
        increaseProductCounter(user);
    }

    public void deleteProductById(Long id) {
        Product product = productRepo.findById(id);
        User user = product.getUser();
        productRepo.delete(product);
        decreaseProductCounter(user);
    }

    public int getMaxQuantityProducts() {
        return maxQuantityProducts;
    }

    public boolean productLimitExceeded(User user) {
        return user.getProductsCounter() >= maxQuantityProducts;
    }

    private void increaseProductCounter(User user) {
        user.setProductsCounter(user.getProductsCounter() + 1);
        userRepo.save(user);
    }

    private void decreaseProductCounter(User user) {
        user.setProductsCounter(user.getProductsCounter() - 1);
        userRepo.save(user);
    }
}
