package ru.ipolynkina.lowPrice2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.domain.dto.CaptchaResponseDto;
import ru.ipolynkina.lowPrice2.service.UserService;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {

    private final static String CAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";

    @Autowired
    private UserService userService;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${recaptcha.secret}")
    private String secret;

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@RequestParam("password2") String password2,
                          @RequestParam("g-recaptcha-response") String captchaResponse,
                          @Valid User user,
                          BindingResult bindingResult,
                          Model model) {

        String url = String.format(CAPTCHA_URL, secret, captchaResponse);
        CaptchaResponseDto response = restTemplate.postForObject(url, Collections.emptyList(), CaptchaResponseDto.class);
        if (!response.isSuccess()) {
            model.addAttribute("captchaError", "Пройдите капчу!");
        }

        boolean passwordIsIncorrect = user.getPassword() == null || !user.getPassword().equals(password2);
        if(passwordIsIncorrect) {
            model.addAttribute("passwordError", "Пароли не идентичны");
        }

        if(passwordIsIncorrect || bindingResult.hasErrors() || !response.isSuccess()) {
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errors);
            return "registration";
        }

        if(!userService.loginIsUnique(user)) {
            model.addAttribute("message", "Пользователь с таким именем уже существует!");
            return "registration";
        }

        if(!userService.emailIsUnique(user)) {
            model.addAttribute("message", "Пользователь с таким email уже существует!");
            return "registration";
        }

        try {
            userService.addUser(user);
            userService.sendActivationCode(user);
            model.addAttribute("message", "Проверьте почту, вам отправлено письмо");
        } catch (MessagingException exc) {
            model.addAttribute("message", "Невозможно отправить сообщение. Повторите попытку позднее.");
            exc.printStackTrace();
        }

        return "login";
    }

    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = userService.activateUser(code);

        if (isActivated) {
            model.addAttribute("message", "Регистрация завершена успешно!");
        } else {
            model.addAttribute("message", "Неверный код активации");
        }

        return "login";
    }
}
