package ru.ipolynkina.lowPrice2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.ProductRepo;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

import java.util.Collections;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ProductRepo productRepo;

    @GetMapping("/users")
    public String login(Model model) {
        fillPageForAdmin(model);
        return "users";
    }

    @PostMapping("/changeStatus")
    public String changeStatus(@RequestParam String username, Model model) {
        User currentUser = userRepo.findByUsername(username);
        currentUser.changeStatus();
        userRepo.save(currentUser);

        fillPageForAdmin(model);
        return "users";
    }

    @PostMapping("/filterByEmail")
    public String filterByEmail(@RequestParam String filter, Model model) {
        filter = filter.trim();
        model.addAttribute("filter", filter);

        User user = userRepo.findByEmail(filter);
        if(user == null) {
            model.addAttribute("users", userRepo.findAll());
        } else {
            model.addAttribute("users", Collections.singletonList(user));
        }

        Iterable<User> users = userRepo.findAll();
        model.addAttribute("usersCounter", users.spliterator().getExactSizeIfKnown());
        return "users";
    }

    @Transactional
    @PostMapping("/deleteAccount")
    public String deleteAccount(@RequestParam String username, Model model) {
        User currentUser = userRepo.findByUsername(username);
        productRepo.deleteAllByUser(currentUser);
        userRepo.delete(currentUser);

        fillPageForAdmin(model);
        return "users";
    }

    private void fillPageForAdmin(Model model) {
        Iterable<User> users = userRepo.findAll();
        model.addAttribute("users", users);
        model.addAttribute("usersCounter", users.spliterator().getExactSizeIfKnown());
    }
}