package ru.ipolynkina.lowPrice2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

@Controller
public class PasswordController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/password")
    public String password() {
        return "password";
    }

    @PostMapping("/changePassword")
    public String changePassword(@AuthenticationPrincipal User user,
                                  @RequestParam(value = "code", required = false) String code,
                                  @RequestParam("password") String password,
                                  @RequestParam("password2") String password2,
                                  Model model) {

        if(!password.equals(password2)) {
            model.addAttribute("passwordError", "Пароли не идентичны!");
            return "password";
        }

        if(password.equals("")) {
            model.addAttribute("passwordError", "Пароль не может быть пустым");
            return "password";
        }

        User currentUser;
        if(code != null) {
            currentUser = userRepo.findByRecoveryCode(code);
        } else {
            currentUser = userRepo.findByUsername(user.getUsername());
        }

        if(currentUser != null) {
            currentUser.setPassword(passwordEncoder.encode(password));
            currentUser.setRecoveryCode(null);
            userRepo.save(currentUser);
            model.addAttribute("message", "Пароль успешно изменен");
        } else {
            model.addAttribute("message", "Код восстановления некорректный!");
        }

        return code == null ? "password" : "login";
    }
}
