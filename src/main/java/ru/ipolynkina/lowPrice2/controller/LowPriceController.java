package ru.ipolynkina.lowPrice2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LowPriceController {

    @GetMapping("/")
    public String main() {
        return "lowPrice";
    }

    @GetMapping("/lowPrice")
    public String lowPrice() {
        return "lowPrice";
    }
}
