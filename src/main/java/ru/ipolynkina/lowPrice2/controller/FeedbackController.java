package ru.ipolynkina.lowPrice2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;
import ru.ipolynkina.lowPrice2.service.UserService;

import javax.mail.MessagingException;

@Controller
public class FeedbackController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserService userService;

    @GetMapping("/feedback")
    public String feedback(@AuthenticationPrincipal User user, Model model) {
        User currentUser = userRepo.findByUsername(user.getUsername());
        model.addAttribute("amountMessages", currentUser.getMessagesCounter());
        return "feedback";
    }

    @PostMapping("/feedback")
    public String sendFeedback(@AuthenticationPrincipal User user,
                               @RequestParam String message,
                               Model model) {

        User currentUser = userRepo.findByUsername(user.getUsername());

        if(userService.messageLimitExceeded(currentUser)) {
            model.addAttribute("message", "Вы не можете отправить более " +
                    userService.getMaxQuantityMessages() + " сообщений в день!");
        } else {
            try {
                userService.sendFeedback(currentUser, message);
                model.addAttribute("message", "Ваше сообщение отправлено!");
            } catch (MessagingException exc) {
                model.addAttribute("message", "Сообщение не отправлено. Повторите попытку позднее");
                exc.printStackTrace();
            }
        }

        model.addAttribute("amountMessages", currentUser.getMessagesCounter());
        return "feedback";
    }
}
