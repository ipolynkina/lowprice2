package ru.ipolynkina.lowPrice2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ipolynkina.lowPrice2.domain.Product;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.parser.PriceParser;
import ru.ipolynkina.lowPrice2.repos.ProductRepo;
import ru.ipolynkina.lowPrice2.repos.UserRepo;
import ru.ipolynkina.lowPrice2.service.ProductService;
import ru.ipolynkina.lowPrice2.service.UserService;

import java.sql.Date;

@Controller("/account")
public class AccountController {

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ProductService productService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserService userService;

    @GetMapping("/account")
    public String account(@AuthenticationPrincipal User user, Model model) {
        User currentUser = userRepo.findByUsername(user.getUsername());
        currentUser.setDateLastVisit(new Date(System.currentTimeMillis()));
        fillPageForUser(userRepo.findByUsername(user.getUsername()), model);
        return "account";
    }

    @PostMapping("/addProduct")
    public synchronized String addProduct(@AuthenticationPrincipal User user,
                             @RequestParam String link,
                             Model model) {

        User currentUser = userRepo.findByUsername(user.getUsername());
        if(productService.productLimitExceeded(currentUser)) {
            model.addAttribute("amountError", "Вы не можете добавить для отслеживания больше " +
                    productService.getMaxQuantityProducts() + " товаров");
        } else if(link != null) {
            PriceParser parser = new PriceParser();
            if(parser.linkIsCorrect(link, model)) {
                parser.saveProductToUser(currentUser);
                productService.addProduct(parser.getSavedProduct(), currentUser);
                userService.setMailingActive(currentUser, true);
            } else model.addAttribute("linkError", "Введенная ссылка некорректна. ");
        }

        fillPageForUser(currentUser, model);
        return "account";
    }

    @PostMapping("/deleteProduct")
    public String deleteProduct(@AuthenticationPrincipal User user,
                                @RequestParam Long id,
                                Model model) {

        User currentUser = userRepo.findByUsername(user.getUsername());
        productService.deleteProductById(id);
        userService.setMailingActive(currentUser, true);

        fillPageForUser(currentUser, model);
        return "account";
    }

    @GetMapping("/stopMailing/{code}")
    public String stopMailing(Model model, @PathVariable String code) {
        User user = userRepo.findByMailingCode(code);

        if (user == null) {
            model.addAttribute("message", "Неверный код. " +
                    "Перейдите по коду из Последнего письма LowPrice.");
        } else {
            userService.setMailingActive(user, false);
            model.addAttribute("message", "Вы успешно отписались от рассылки!");
        }

        return "login";
    }

    private void fillPageForUser(User currentUser, Model model) {
        Iterable<Product> products = productRepo.findByUser(currentUser);
        model.addAttribute("products", products);
        model.addAttribute("productsCounter", userService.getAmountProducts(currentUser));
    }
}
