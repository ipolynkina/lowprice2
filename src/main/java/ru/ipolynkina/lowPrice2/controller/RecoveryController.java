package ru.ipolynkina.lowPrice2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.ipolynkina.lowPrice2.domain.Role;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;
import ru.ipolynkina.lowPrice2.service.UserService;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.UUID;

@Controller
public class RecoveryController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserService userService;

    @GetMapping("/recovery")
    public String recovery() {
        return "recovery";
    }

    @PostMapping("/recovery")
    public String recoveryPassword(@Valid String email, Model model) {
        User user = userRepo.findByEmail(email);

        try {
            if(user == null) {
                model.addAttribute("message", "Пользователь с таким email не найден");
            } else if(user.getRoles().contains(Role.DEMO)) {
                model.addAttribute("message", "Изменить пароль для данной учетной записи невозможно!");
            } else {
                user.setRecoveryCode(UUID.randomUUID().toString());
                userRepo.save(user);
                userService.sendRecoveryCode(user);
                model.addAttribute("message", "Вам на почту отправлено письмо. Следуйте инструкции.");
            }
        } catch (MessagingException exc) {
            model.addAttribute("message", "Сообщение не отправлено. Повторите попытку позднее.");
            exc.printStackTrace();
        }

        return "recovery";
    }

    @GetMapping("/recovery/{code}")
    public String getNewPassword(Model model, @PathVariable String code) {
        User currentUser = userRepo.findByRecoveryCode(code);
        if(currentUser == null) {
            model.addAttribute("message", "Код восстановления некорректный!");
        } else {
            model.addAttribute("code", code);
        }
        return "password";
    }
}
