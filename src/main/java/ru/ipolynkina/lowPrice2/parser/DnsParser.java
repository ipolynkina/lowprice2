package ru.ipolynkina.lowPrice2.parser;

public class DnsParser extends Parser {

    public static final String WEBSITE_DNS = "dns";

    private static final String priceTag = "span.current-price-value";
    private static final String nameTag = "h1.page-title.price-item-title";

    public DnsParser() {
        super(priceTag, nameTag);
    }
}
