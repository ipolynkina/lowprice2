package ru.ipolynkina.lowPrice2.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.ipolynkina.lowPrice2.domain.Product;
import ru.ipolynkina.lowPrice2.domain.User;

import java.io.IOException;

public class Parser {

    private Product product = new Product();
    private String nameProduct;
    private String linkProduct;
    private double priceProduct;

    private String priceTag;
    private String nameTag;

    public Parser(String priceTag, String nameTag) {
        this.priceTag = priceTag;
        this.nameTag = nameTag;
    }

    public boolean linkIsCorrect(String link) {
        try {
            Document doc = getDocument(link);
            priceProduct = getPriceProduct(doc);
            nameProduct = getNameProduct(doc);
            this.linkProduct = link;
        } catch (IOException | IndexOutOfBoundsException exc) {
            exc.printStackTrace();
            return false;
        }
        return true;

    }

    public void saveProductToUser(User user) {
        product = new Product(nameProduct, linkProduct, priceProduct, user);
    }

    public Product getSavedProduct() {
        return product;
    }

    protected String getNameProduct() {
        return nameProduct;
    }

    protected void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    private Document getDocument(String link) throws IOException {
        return Jsoup.connect(link).get();
    }

    private Double getPriceProduct(Document doc) throws IndexOutOfBoundsException {
        Elements prices = doc.select(priceTag);
        String textPrice = prices.get(0).text();
        textPrice = textPrice.replaceAll("\\D+", "");
        return Double.parseDouble(textPrice);
    }

    private String getNameProduct(Document doc) throws IndexOutOfBoundsException {
        Elements names = doc.select(nameTag);
        return names.get(0).text();
    }
}
