package ru.ipolynkina.lowPrice2.parser;

public class EldoradoParser extends Parser {

    public static final String WEBSITE_ELDORADO = "eldorado";

    private static final String priceTag = "div.product-box-price__active";
    private static final String nameTag = "h1.catalogItemDetailHd";

    public EldoradoParser() {
        super(priceTag, nameTag);
    }

    @Override
    public boolean linkIsCorrect(String link) {
        link = link.replace("today_only/?id=", "cat/detail/");
        return super.linkIsCorrect(link);
    }
}
