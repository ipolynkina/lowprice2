package ru.ipolynkina.lowPrice2.parser;

public class MvideoParser extends Parser {

    public static final String WEBSITE_MVIDEO = "mvideo";

    private static final String priceTag = "div.c-pdp-price__current.sel-product-tile-price";
    private static final String nameTag = "h1.e-h1.sel-product-title";

    public MvideoParser() {
        super(priceTag, nameTag);
    }
}
