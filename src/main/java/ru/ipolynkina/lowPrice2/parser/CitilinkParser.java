package ru.ipolynkina.lowPrice2.parser;

import ru.ipolynkina.lowPrice2.domain.User;

public class CitilinkParser extends Parser {

    public static final String WEBSITE_CITILINK = "citilink";

    private static final String priceTag = "div.price.price_break";
    private static final String nameTag = "div.product_header";

    public CitilinkParser() {
        super(priceTag, nameTag);
    }

    @Override
    public void saveProductToUser(User user) {
        String nameProduct = getNameProduct();
        setNameProduct(nameProduct.replace("шт. ", ""));
        super.saveProductToUser(user);
    }
}
