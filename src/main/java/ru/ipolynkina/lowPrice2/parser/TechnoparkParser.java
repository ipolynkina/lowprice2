package ru.ipolynkina.lowPrice2.parser;

public class TechnoparkParser extends Parser {

    public static final String WEBSITE_TECHNOPARK = "technopark";

    private static final String priceTag = "div.price-tag-cost-final";
    private static final String nameTag = "h1.product-name";

    public TechnoparkParser() {
        super(priceTag, nameTag);
    }
}
