package ru.ipolynkina.lowPrice2.parser;

import org.springframework.ui.Model;
import ru.ipolynkina.lowPrice2.domain.Product;
import ru.ipolynkina.lowPrice2.domain.User;

public class PriceParser {

    private Parser parser;

    public boolean linkIsCorrect(String link, Model model) {
        giveInfo(link, model);

        if(link.contains(MvideoParser.WEBSITE_MVIDEO)) {
            parser = new MvideoParser();
        } else if(link.contains(TechnoparkParser.WEBSITE_TECHNOPARK)) {
            parser = new TechnoparkParser();
        } else if(link.contains(CitilinkParser.WEBSITE_CITILINK)) {
            parser = new CitilinkParser();
        } else {
            return false;
        }

        return parser.linkIsCorrect(link);
    }

    public void saveProductToUser(User user) {
        parser.saveProductToUser(user);
    }

    public Product getSavedProduct() {
        return parser.getSavedProduct();
    }

    private void giveInfo(String link, Model model) {
        if(model == null) return;
        if(!link.contains(EldoradoParser.WEBSITE_ELDORADO) && !link.contains(DnsParser.WEBSITE_DNS)) return;

        model.addAttribute("amountError", "На данный момент отслеживание цен работает только для сайтов: " +
                "М.Видео, Технопарк и Ситилинк");
    }
}
