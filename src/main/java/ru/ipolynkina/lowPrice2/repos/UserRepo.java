package ru.ipolynkina.lowPrice2.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ipolynkina.lowPrice2.domain.User;

public interface UserRepo extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByEmail(String email);

    User findByActivationCode(String code);

    User findByRecoveryCode(String code);

    User findByMailingCode(String code);
}
