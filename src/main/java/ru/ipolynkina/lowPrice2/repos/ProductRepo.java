package ru.ipolynkina.lowPrice2.repos;

import org.springframework.data.repository.CrudRepository;
import ru.ipolynkina.lowPrice2.domain.Product;
import ru.ipolynkina.lowPrice2.domain.User;

public interface ProductRepo extends CrudRepository<Product, Integer> {

    Product findById(Long id);

    void deleteAllByUser(User user);

    Iterable<Product> findByUser(User user);
}
