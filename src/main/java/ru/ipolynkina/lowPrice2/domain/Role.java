package ru.ipolynkina.lowPrice2.domain;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    DEMO, USER, ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }
}
