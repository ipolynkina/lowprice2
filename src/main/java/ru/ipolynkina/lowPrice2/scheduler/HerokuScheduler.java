package ru.ipolynkina.lowPrice2.scheduler;

import org.jsoup.Jsoup;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class HerokuScheduler {

    @Scheduled(cron = "0 0/30 8-20 * * *")
    public void sendRequest() {
        try {
            String url = "https://low-price.herokuapp.com";
            Jsoup.connect(url).get();
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
