package ru.ipolynkina.lowPrice2.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

import java.util.List;

@Component
public class AccountScheduler {

    @Autowired
    private UserRepo userRepo;

    @Scheduled(cron = "0 0 0 * * *")
    public void deleteUnverifiedAccounts() {
        List<User> users = userRepo.findAll();
        for(User user : users) {
            if(user.getActivationCode() != null) {
                userRepo.delete(user);
            }
        }
    }
}
