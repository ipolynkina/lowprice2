package ru.ipolynkina.lowPrice2.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.ipolynkina.lowPrice2.domain.Product;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.parser.PriceParser;
import ru.ipolynkina.lowPrice2.repos.ProductRepo;
import ru.ipolynkina.lowPrice2.repos.UserRepo;
import ru.ipolynkina.lowPrice2.service.UserService;

import javax.mail.MessagingException;
import java.util.List;
import java.util.UUID;

@Component
public class PriceScheduler {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductRepo productRepo;

    @Value("${hostname}")
    private String hostname;

    @Scheduled(fixedRate = 1_000 * 60 * 60)
    public void checkPrices() throws MessagingException {
        List<User> users = userRepo.findAll();
        for(User user : users) {
            boolean pricesChanged = false;
            String message = user.getUsername() + "! Цены изменились! <br/>";
            Iterable<Product> products = productRepo.findByUser(user);

            for(Product product : products) {
                double lastPrice = product.getPrice();
                PriceParser parser = new PriceParser();

                if(parser.linkIsCorrect(product.getLink(), null)) {
                    parser.saveProductToUser(user);
                    double newPrice = parser.getSavedProduct().getPrice();
                    if(lastPrice != newPrice) {
                        pricesChanged = true;
                        message = addInfo(message, product, lastPrice, newPrice);
                        product.setPrice(newPrice);
                        productRepo.save(product);
                    }
                }
            }
            if(pricesChanged && user.isMailingActive()) {
                user.setMailingCode(UUID.randomUUID().toString());
                userRepo.save(user);
                userService.sendPricingInformation(user, message);

            }
        }
    }

    private String addInfo(String message, Product product, double lastPrice, double newPrice) {
        return message + String.format("<br/> %s " +
                        "<br/> Старая цена %s " +
                        "<br/> Новая цена %s " +
                        "<br/> <a href=\"%s\" target=\"_blank\">Перейти к товару</a> <br/>",
                product.getName(),
                lastPrice,
                newPrice,
                product.getLink()
        );
    }
}
