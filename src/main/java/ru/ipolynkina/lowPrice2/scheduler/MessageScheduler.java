package ru.ipolynkina.lowPrice2.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

import java.util.List;

@Component
public class MessageScheduler {

    @Autowired
    private UserRepo userRepo;

    @Scheduled(cron = "0 0 1 * * *")
    public void messageCounterUpdate() {
        List<User> users = userRepo.findAll();
        for(User user : users) {
            user.setMessagesCounter(0);
            userRepo.save(user);
        }
    }
}
