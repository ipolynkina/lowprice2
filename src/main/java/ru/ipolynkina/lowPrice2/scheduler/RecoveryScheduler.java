package ru.ipolynkina.lowPrice2.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.ipolynkina.lowPrice2.domain.User;
import ru.ipolynkina.lowPrice2.repos.UserRepo;

import java.util.List;

@Component
public class RecoveryScheduler {

    @Autowired
    private UserRepo userRepo;

    @Scheduled(fixedDelay = 1_000 * 60 * 15)
    public void deleteRecoveryCode() {
        List<User> users = userRepo.findAll();
        for(User user : users) {
            if(user.getRecoveryCode() != null) {
                user.setRecoveryCode(null);
                userRepo.save(user);
            }
        }
    }
}
