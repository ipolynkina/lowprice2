package ru.ipolynkina.lowPrice2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LowPrice2 {

    public static void main(String[] args) {
        SpringApplication.run(LowPrice2.class, args);
    }
}
