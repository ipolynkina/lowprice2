create sequence hibernate_sequence start 1 increment 1;

create table product (
    id int8 not null,
    link varchar(255),
    name varchar(255),
    price float8,
    user_id int8,
    primary key (id)
);

create table user_role (
    user_id int8 not null,
    roles varchar(255)
);

create table usr (
    id int8 not null,
    activation_code varchar(255),
    date_last_visit date,
    email varchar(255),
    mailing_active boolean not null,
    mailing_code varchar(255),
    messages_counter int4 not null,
    password varchar(255),
    products_counter int4 not null,
    recovery_code varchar(255),
    user_active boolean not null,
    username varchar(255),
    vip_level int4 not null,
    primary key (id)
);

alter table if exists product
    add constraint product_user_fk
    foreign key (user_id) references usr;

alter table if exists user_role
    add constraint user_role_fk
    foreign key (user_id) references usr;