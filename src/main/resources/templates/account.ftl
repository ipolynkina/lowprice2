<#import "parts/common.ftl" as c>

<@c.page>
    <div class="container">
        <h2>Товары для отслеживания (${productsCounter})</h2>
    </div>
    <#if amountError??>
        <div class="alert alert-secondary" role="alert">
            ${amountError}
        </div>
    </#if>
    <#if linkError??>
        <div class="alert alert-secondary" role="alert">
            ${linkError}
        </div>
    </#if>
    <div class="container">
        <form method="post" action="addProduct" class="container">
            <div class="form-group row">
                <input type="text" name="link" class="form-control col-sm-6"
                       placeholder="Вставьте ссылку на товар"> </input>
                <input type="hidden" name="_csrf" value="${_csrf.token}"> </input>
                <div class="col-sm-1">
                    <button class="btn btn-outline-secondary" type="submit">Добавить</button>
                </div>
            </div>
        </form>
    </div>
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col">Название</th>
                <th scope="col">Цена</th>
                <th scope="col">Ссылка</th>
                <th scope="col">Действие</th>
            </tr>
            </thead>
            <tbody class="table table-striped">
            <#list products as product>
                <tr>
                    <form method="post" action="deleteProduct" id="productList">
                        <th scope="row"><input type="hidden" name="id" value="${product.id}"></input></th>
                        <th scope="row"><input type="hidden" name="user" value="${product.user}"></input></th>
                        <td>${product.name}</td>
                        <td>${product.price}</td>
                        <td><a href="${product.link}" target="_blank" class="text-dark">Перейти к товару</a></td>
                        <input type="hidden" name="_csrf" value="${_csrf.token}"></input>
                        <td><button class="btn btn-outline-secondary" type="submit">Удалить</button></td>
                    </form>
                </tr>
            </#list>
            </tbody>
        </table>
    </div>
</@c.page>