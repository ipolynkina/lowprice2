<#import "parts/common.ftl" as c>

<@c.page>
    <#if message??>
        <div class="alert alert-secondary" role="alert">
            ${message?ifExists}
        </div>
    </#if>
    <div>
        <form method="post" action="/registration">
            <div class="form-group row">
                <label class="col-sm-1 col-form-label">Логин</label>
                <div class="col-sm-4">
                    <input type="text" name="username" value="<#if user??>${user.username}</#if>"
                           class="form-control ${(usernameError??)?string('is-invalid', '')}"
                           placeholder="Логин" />
                    <#if usernameError??>
                        <div class="invalid-feedback">
                            ${usernameError}
                        </div>
                    </#if>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 col-form-label">Email</label>
                <div class="col-sm-4">
                    <input type="text" name="email" value="<#if user??>${user.email}</#if>"
                           class="form-control ${(emailError??)?string('is-invalid', '')}"
                           placeholder="Email"/>
                    <#if emailError??>
                        <div class="invalid-feedback">
                            ${emailError}
                        </div>
                    </#if>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 col-form-label">Пароль</label>
                <div class="col-sm-4">
                    <input type="password" name="password"
                           class="form-control ${(passwordError??)?string('is-invalid', '')}"
                           placeholder="Пароль"/>
                    <#if passwordError??>
                        <div class="invalid-feedback">
                            ${passwordError}
                        </div>
                    </#if>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 col-form-label">Пароль</label>
                <div class="col-sm-4">
                    <input type="password" name="password2"
                           class="form-control ${(password2Error??)?string('is-invalid', '')}"
                           placeholder="Пароль (еще раз)"/>
                    <#if password2Error??>
                        <div class="invalid-feedback">
                            ${password2Error}
                        </div>
                    </#if>
                </div>
            </div>
            <div class="form-group row">
                <div class="g-recaptcha" data-sitekey="6LeFI7kUAAAAAPXhqOW6c0ewkRdaCM3KX-9l1VsF"></div>
                <#if captchaError??>
                    <div class="alert alert-danger" role="alert">
                        ${captchaError}
                    </div>
                </#if>
            </div>
            <div class="form-group row">
                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                <button class="btn btn-outline-secondary" type="submit">Зарегистрироваться</button>
            </div>
        </form>
    </div>
</@c.page>