<#import "parts/common.ftl" as c>

<@c.page>
    <#if message??>
        <div class="alert alert-secondary" role="alert">
            ${message?ifExists}
        </div>
    </#if>
        <div class="container">
            <form method="post" action="/changePassword">
                <div class="form-group row">
                    <label class="col-sm-1,5 col-form-label">Новый пароль</label>
                    <div class="col-sm-4">
                        <input type="password" name="password"
                               class="form-control ${(passwordError??)?string('is-invalid', '')}"
                               placeholder="Новый пароль"/>
                        <#if passwordError??>
                            <div class="invalid-feedback">
                                ${passwordError}
                            </div>
                        </#if>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-1,5 col-form-label">Новый пароль</label>
                    <div class="col-sm-4">
                        <input type="password" name="password2"
                               class="form-control ${(password2Error??)?string('is-invalid', '')}"
                               placeholder="Новый пароль (еще раз)"/>
                        <#if password2Error??>
                            <div class="invalid-feedback">
                                ${password2Error}
                            </div>
                        </#if>
                    </div>
                </div>
                <div class="form-group row">
                    <#if code??>
                        <input type="hidden" name="code" value="${code?ifExists}"/>
                    </#if>
                    <input type="hidden" name="_csrf" value="${_csrf.token}" />
                    <button class="btn btn-outline-secondary" type="submit">Сохранить</button>
                </div>
            </form>
        </div>
</@c.page>