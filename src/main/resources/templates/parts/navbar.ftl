<#include "security.ftl">

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Low Price</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/account">Личный кабинет</a>
            </li>
            <#if isAdmin>
                <li class="nav-item">
                    <a class="nav-link" href="/users">Пользователи</a>
                </li>
            </#if>
            <#if isAdmin>
                <li class="nav-item">
                    <a class="nav-link" href="/password">Изменить пароль</a>
                </li>
            </#if>
            <li class="nav-item">
                <a class="nav-link" href="/feedback">Обратная связь</a>
            </li>
        </ul>
    </div>

    <div class="navbar-text mr-3"> ${name} </div>

    <div class="text-right">
        <form action="/logout" method="post">
            <input type="hidden" name="_csrf" value="${_csrf.token}" />
            <button class="btn btn-outline-secondary" type="submit"><#if user??>Выйти<#else>Войти</#if></button>
        </form>
    </div>
</nav>
