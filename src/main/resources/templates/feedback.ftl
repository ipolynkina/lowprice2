<#import "parts/common.ftl" as c>

<@c.page>
    <#if message??>
        <div class="alert alert-secondary" role="alert">
            ${message?ifExists}
        </div>
    </#if>
    <div class="container">
        <div class="mb-3">
            <div id="amountMessages">Сегодня отправлено сообщений: ${amountMessages} </div>
        </div>
        <div>
            <form action="/feedback" method="post">
                <div class="row">
                    <div class="col-sm-7 mb-3">
                        <textarea class="form-control" name="message" rows="7"></textarea>
                    </div>
                    <div class="mb-3">
                        <input type="hidden" name="_csrf" value="${_csrf.token}" ></input>
                        <button class="btn btn-outline-secondary" type="submit">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</@c.page>