<#import "parts/common.ftl" as c>

<@c.page>
<div class="shadow-lg p-3 mb-3 bg-white rounded">
    Планируете покупку в интернет-магазинах М.Видео, Технопарк или Ситилинк?<br/>
    Ждёте, пока цена на товар снизится?
</div>
<div class="shadow-lg p-3 mb-3 bg-white rounded">
    Не тратьте свое время!<br/>
    Мы оповестим вас об изменении цены!
</div>
<div class="shadow-lg p-3 mb-3 bg-white rounded">
    Вам нужно сделать всего три простых шага:
    <ol>
        <li>Зарегистрироваться на сайте</li>
        <li>Подтвердить свой email</li>
        <li>Указать товар, которым вы интересуетесь</li>
    </ol>
</div>
<div class="shadow-lg p-3 mb-3 bg-white rounded">
    Оповещение об изменении цены мы пришлем вам на email.<br/>
    И гарантируем: никакого спама!
</div>
<div class="shadow-lg p-3 mb-3 bg-white rounded">
    Если вы хотите отслеживать цены в магазинах других компаний - напишите нам об этом<br/>
    Возможно, что в ближайшее время такая возможность появится =)
</div>
</@c.page>