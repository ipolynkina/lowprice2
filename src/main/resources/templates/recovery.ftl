<#import "parts/common.ftl" as c>

<@c.page>
    <div class="form-group col">
        <div class="shadow-lg p-3 mb-3 bg-white rounded">
            Укажите свой email, и мы отправим на него инструцию для восстановления пароля
        </div>
        <#if message??>
            <div class="alert alert-secondary" role="alert">
                ${message?ifExists}
            </div>
        </#if>
        <div class="form-group col">
            <form action="/recovery" method="post">
                <div class="form-group row">
                    <label class="col-sm-1 col-form-label">Email</label>
                    <div class="col-sm-6">
                        <input type="text" name="email"
                               class="form-control ${(emailError??)?string('is-invalid', '')}"
                               placeholder="Укажите свой email"/>
                        <#if emailError??>
                            <div class="invalid-feedback">
                                ${emailError}
                            </div>
                        </#if>
                    </div>
                    <input type="hidden" name="_csrf" value="${_csrf.token}" />
                    <button class="btn btn-outline-secondary" type="submit">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</@c.page>