<#import "parts/common.ftl" as c>

<@c.page>
    <div class="container">
        <h2>Список пользователей (${usersCounter})</h2>
    </div>
    <div class="container">
        <form method="post" action="filterByEmail">
            <div class="form-group row">
                <div class="col-sm-4">
                    <input type="text" name="filter" class="form-control" value="${filter?ifExists}" />
                </div>
                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                <button class="btn btn-outline-secondary" type="submit">Найти по email</button>
            </div>
        </form>
    </div>
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Логин</th>
                <th scope="col">Email</th>
                <th scope="col">Товаров</th>
                <th scope="col">Роли</th>
                <th scope="col">Учетка активна</th>
                <th scope="col">Рассылка активна</th>
                <th scope="col">Последнее посещение</th>
                <th scope="col">Действия</th>
                <th></th>
            </tr>
            </thead>
            <tbody class="table table-striped">
            <#list users as user>
                <tr>
                    <form method="post" action="changeStatus" id="userList">
                        <th scope="row"><input type="hidden" name="id" value="${user.id}"/></th>
                        <td>${user.username}</td>
                        <td>${user.email}</td>
                        <td>${user.productsCounter}</td>
                        <td><#list user.roles as role>${role}<#sep>, </#list></td>
                        <td id="status/${user.id}"><#if user.isUserActive()>Да<#else>Нет</#if></td>
                        <td><#if user.isMailingActive()>Да<#else>Нет</#if></td>
                        <td><#if user.dateLastVisit??>${user.dateLastVisit}</#if></td>
                        <input type="hidden" name="username" value="${user.username}" />
                        <input type="hidden" name="_csrf" value="${_csrf.token}" />
                        <td><button class="btn btn-outline-secondary" type="submit">
                                <#if user.isUserActive()>Выключить<#else>Включить</#if></button></td>
                    </form>
                    <form method="post" action="deleteAccount">
                        <input type="hidden" name="username" value="${user.username}" />
                        <input type="hidden" name="_csrf" value="${_csrf.token}" />
                        <td><button class="btn btn-outline-secondary" type="submit">Удалить</button></td>
                    </form>
                </tr>
            </#list>
            </tbody>
        </table>
    </div>
</@c.page>