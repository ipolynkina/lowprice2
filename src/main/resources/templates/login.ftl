<#import "parts/common.ftl" as c>

<@c.page>
    <#if Session?? && Session.SPRING_SECURITY_LAST_EXCEPTION??>
        <div class="alert alert-secondary" role="alert">
            ${Session.SPRING_SECURITY_LAST_EXCEPTION.message}
        </div>
    </#if>
    <#if message??>
        <div class="alert alert-secondary" role="alert">
            ${message?ifExists}
        </div>
    </#if>
    <div>
        <div>
            <form action="/login" method="post">
                <div class="form-group row">
                    <label class="col-sm-1 col-form-label">Логин</label>
                    <div class="col-sm-4">
                        <input type="text" name="username" class="form-control" placeholder="Логин"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-1 col-form-label">Пароль</label>
                    <div class="col-sm-4">
                        <input type="password" name="password" class="form-control" placeholder="Пароль"/>
                    </div>
                </div>
                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                <button class="btn btn-outline-secondary" type="submit">Войти</button>
            </form>
            <br/><a class="text-secondary" href="/registration">Я новый пользователь</a>
            <br/><a class="text-secondary" href="/recovery">Я забыл пароль</a>
        </div>
    </div>
</@c.page>